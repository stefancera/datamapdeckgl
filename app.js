/* global window,document,fetch */
import React, {Component} from 'react';
import {render} from 'react-dom';
import {MapController} from 'deck.gl/dist/controllers';
import DeckGL, {GeoJsonLayer} from 'deck.gl';
import './style.css';

const GEOJSONSTATES = process.env.NODE_ENV === 'production' ? 'https://s3.eu-central-1.amazonaws.com/s3-cera/datamap/states.geojson' : 'data/states.geojson';
const GEOJSONDATA = process.env.NODE_ENV === 'production' ? 'https://s3.eu-central-1.amazonaws.com/s3-cera/datamap/us_state_zcta_2014_final.geojson' : 'data/us_state_zcta_2014_final.geojson';


class Root extends Component {

    constructor(props) {
        super(props);
        this.state = {
            viewport: {
                latitude: 40,
                longitude: -100,
                zoom: 3,
                bearing: 0,
                pitch: 0
            },
            width: 960,
            height: 600,
            data: null
        };

        fetch(GEOJSONSTATES)
            .then(resp => resp.json())
            .then(states => this.setState({states}));
        fetch(GEOJSONDATA)
            .then(res => res.json())
            .then(data => this.setState({data}));
    }

    componentDidMount() {
        // window.addEventListener('resize', this._resize.bind(this));
        // this._resize();
    }

    _resize() {
        this.setState({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    _renderTooltip() {
        const {hoveredFeature, hoveredX, hoveredY} = this.state;

        return hoveredFeature && (
            <div className="tooltip" style={{left: hoveredX, top: hoveredY}}>
                <div>Median Household Income: ${hoveredFeature.properties.value} in ZIP Code: {hoveredFeature.properties.zcta}</div>
            </div>
        );
    };

    _renderLoader() {
        const {doneInitializing} = this.state;
        return !doneInitializing && (
            <div className="loader-wrapper">
                <div className="loader">Loading...</div>
            </div>
        );
    };


    render() {
        const {viewport, width, height, data, states} = this.state;

        const onHover = (event) => {
            const {object, x, y} = event;
            const hoveredFeature = object;
            this.setState({hoveredFeature, hoveredX: x, hoveredY: y});
        };

        const onWebGLInitialized = () => {
            console.log('initialized');
            this.setState({
                doneInitializing: true
            });
        };


        return (

            <div>
                <h1>Median Household Income by ZCTA, 2014</h1>
                <div className="map">
                    <MapController
                        {...viewport}
                        width={width}
                        height={height}
                        onViewportChange={v => this.setState({viewport: v})}>
                        <div>

                        </div>
                        {this._renderTooltip()}
                        {this._renderLoader()}
                        <DeckGL
                            {...viewport}
                            width={width}
                            height={height}
                            debug={false}
                            useDevicePixelRatio={false}
                            onWebGLInitialized={ onWebGLInitialized }
                            layers={[
                                new GeoJsonLayer({
                                    id: 'data-layer',
                                    data,
                                    stroked: true,
                                    filled: true,
                                    pickable: true,
                                    getLineColor: (f) => [255, 255, 255],
                                    getFillColor: (f) => f.properties.fillColor || [23, 162, 52],
                                    onHover: onHover,
                                }),
                                new GeoJsonLayer({
                                    id: 'states-layer-borders',
                                    data: states,
                                    stroked: true,
                                    filled: false,
                                    lineWidthMinPixels: 1,
                                    getLineColor: () => [255, 255, 255]
                                }),
                            ]}/>
                    </MapController>
                </div>
            </div>
        );
    }
}

render(<Root/>, document.body.appendChild(document.createElement('div')));