const range=require('d3-array').range;
const scaleQuantile=require('d3-scale').scaleQuantile;

const percentileColor = [
   [218, 236, 220],
   [197, 226, 201],
   [176, 216, 182],
   [155, 207, 163],
   [134, 197, 144],
   [113, 187, 125],
   [92, 178, 106],
   [71, 168, 87],
   [50, 159, 69]
];


exports.updatePercentiles = function(featureCollection, accessor) {
    const {features} = featureCollection;
    let dataBar = require('./progressBar').bar('data', '' , features.length, 'Calculating additional properties');
    console.log('Starting scale');
    const scale = scaleQuantile().domain(features.map(accessor)).range(range(9));
    console.log('Done scale');
    features.forEach(f => {
        f.properties.percentile = scale(f.properties.value);
        f.properties.fillColor = percentileColor[f.properties.percentile];
        dataBar.tick();
    });
    console.log('done with properties');
};

