/*
*
* Check number of csv lines for progress bar
*
* */
const fs = require('fs');
const ProgressBar = require('node-progress-bars');

exports.bar = function (type, csvFilePath, sentCount, schemaMessage) {
    let count = sentCount;

    if (type === 'csv') {
        let data = fs.readFileSync(csvFilePath);
        count = data.toString().split("\n") - 1;
    }
    console.log(count);

    return new ProgressBar({ schema: schemaMessage + ' :bar :current/:total :percent', total: count });
};
