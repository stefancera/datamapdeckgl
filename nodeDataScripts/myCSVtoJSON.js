const csvFilePath='datamapData/DATA_mhi_us_zcta_2014.csv';
const csv=require('csvtojson');

const color=require('./color.js');

const jsonfile = require('jsonfile');
const geojsonFilePath = 'myData/us_state_zcta_2014.geojson';
const geojsonFinalFilePath = 'myData/us_state_zcta_2014_final.geojson';

let finalGeojsonObj = {};

let csvBar = require('./progressBar').bar('csv', csvFilePath, 0, 'Adding CSV data');

jsonfile.readFile(geojsonFilePath, function(err, geojsonObj) {
    finalGeojsonObj = geojsonObj;
    csv()
        .fromFile(csvFilePath)
        .on('json',(jsonObj)=>{
            const {features} = finalGeojsonObj;

            let foundIndex = features.findIndex(f => f.properties.id == jsonObj.id);
            finalGeojsonObj.features[foundIndex].properties.value = jsonObj.mhi;
            finalGeojsonObj.features[foundIndex].properties.zcta = jsonObj.zcta;
            csvBar.tick(1);
        })
        .on('done',(error)=>{
            console.log('Done with CSV');
            color.updatePercentiles(finalGeojsonObj, f => f.properties.value);
            console.log('Saving to file');
            jsonfile.writeFile(geojsonFinalFilePath, finalGeojsonObj, function (err) {
                err ? console.error(err) : console.log('Finished');
            });
        });

});


