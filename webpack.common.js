// avoid destructuring for older Node version support
const resolve = require('path').resolve;
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


const CONFIG = {
    entry: {
        app: resolve('./app.js')
    },

    devtool: 'source-map',

    module: {
        rules: [{
            // Compile ES2015 using buble
            test: /\.js$/,
            loader: 'buble-loader',
            include: [resolve('.')],
            exclude: [/node_modules/],
            options: {
                objectAssign: 'Object.assign'
            }
        },
            {
                test: /\.css$/,
                use: [
                    { loader: "style-loader/url" },
                    { loader: "file-loader" }
                ]
            }
        ]
    },

    resolve: {
        alias: {
            'viewport-mercator-project': resolve('./node_modules/viewport-mercator-project/dist/index.js')
        }
    },

    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            "files": {
                "css": [ "style.css" ]
            }
        }),
        new CopyWebpackPlugin([
                { from: 'myData/us_state_zcta_2014_final.geojson', to: 'data/us_state_zcta_2014_final.geojson'},
                { from: 'myData/states.geojson', to: 'data/states.geojson'},
        ])
    ],
    output: {
        filename: '[name].bundle.js',
        path: resolve(__dirname, 'dist')
    }
};

// This line enables bundling against src in this repo rather than installed deck.gl module
module.exports = CONFIG;